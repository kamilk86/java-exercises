# Read Me
An application serves an endpoint:

GET /org/{organization_name}/contributors

which returns a list sorted by the number of contributions made by the developer to all repositories for the
given organization.

An example call of locally running application:
http://localhost:8080/org/sevenwire/contributors

Swagger docs available on:
http://localhost:8080/swagger-ui/#/contributor-controller

The following libraries/tools were used:

- gradle 
- spring-boot
- fasterxml.jackson