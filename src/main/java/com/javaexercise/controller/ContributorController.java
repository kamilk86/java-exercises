package com.javaexercise.controller;

import com.javaexercise.model.Contributor;
import com.javaexercise.model.Repository;
import com.javaexercise.service.ProcessData;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

@RestController
public class ContributorController {

    @Autowired
    private ProcessData processData;

    @ApiOperation(httpMethod = "GET", value = "Get list of Contributions with sum of contributions for each " +
            "repository for a given organisation, sorted descending")
    @ApiParam(name = "orgName", value = "organisation name", required = true)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "success"),
            @ApiResponse(code = 500, message = "an error occurred")
    })

    @GetMapping("/org/{orgName}/contributors")
    public List<Contributor> contributors(@PathVariable String orgName) {

        List<Repository> repositories = processData.getReporitoriesByOrg(orgName);
        List<CompletableFuture<List<Contributor>>> completableRawContributors = processData.getRawContributors(orgName, repositories);

        List<Contributor> rawContributors = processData.completeContributorsRequest(completableRawContributors);

        Map<String, List<Integer>> contributorToContributions = processData.groupByRawContributorsByName(rawContributors);

        return processData.reduceContributionsAndSort(contributorToContributions);
    }

    // For UT purpose
    public void setProcessData(ProcessData processData) {
        this.processData = processData;
    }
}