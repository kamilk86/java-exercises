package com.javaexercise.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.javaexercise.model.Contributor;
import com.javaexercise.model.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.http.HttpResponse;
import java.util.LinkedList;
import java.util.List;

@Service
public class Serializer {

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * Serializes a HTTP response to a list of Repository or Contributor.
     * @param response
     * @param clazz - points what type of response object is expected, Repository or Contributor
     * @return list of Repository or Contributor
     * @throws JsonProcessingException
     */
    public List serialize(HttpResponse<String> response, Class clazz) throws JsonProcessingException {
        if (clazz.equals(Repository.class)) {
            return objectMapper.readValue(response.body(), new TypeReference<List<Repository>>() {
            });
        } else if (clazz.equals(Contributor.class)) {
            return objectMapper.readValue(response.body(), new TypeReference<List<Contributor>>() {
            });
        }
        return new LinkedList();
    }

    // For UT purpose
    public void setObjectMapper(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }
}
