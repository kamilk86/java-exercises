package com.javaexercise.service;

import com.javaexercise.model.Contributor;
import com.javaexercise.model.Repository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

@Service
public class ProcessData {

    public static final String DOMAIN = "https://api.github.com/";

    @Autowired
    private RequestService requestService;

    /**
     * Sends a request about repositories connected with a given organization name
     * @param orgName organization name
     * @return a list of Repository objects, connected with a given organization name
     */
    public List<Repository> getReporitoriesByOrg(String orgName) {
        try {
            return requestService.callPaginatedRequest(DOMAIN + "orgs/" + orgName + "/repos", Repository.class);
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
            throw new ResponseStatusException(
                    HttpStatus.INTERNAL_SERVER_ERROR, "An error occurred while getting repositories", e);
        }
    }

    /**
     * Sends a request about contributors who contribute on each passed repository and organisation name
     * @param orgName organization name
     * @param repositories
     * @return a list of CompletableFuture request about contributors
     */
    public List<CompletableFuture<List<Contributor>>> getRawContributors(String orgName, List<Repository> repositories){
        return repositories.stream().map(repo -> {
            try {
                return getContributorsByOrgAndRepo(orgName, repo.getName());
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
                throw new ResponseStatusException(
                        HttpStatus.INTERNAL_SERVER_ERROR, "An error occurred while getting contributors", e);
            }
        }).collect(Collectors.toList());
    }

    /**
     * Completes a async request about contributors data.
     * @param completableRawContributors
     * @return a list of Contributor objects. These objects are not group in any way.
     */
    public List<Contributor> completeContributorsRequest(List<CompletableFuture<List<Contributor>>> completableRawContributors) {
        CompletableFuture.allOf(completableRawContributors.toArray(new CompletableFuture[completableRawContributors.size()])).join();
        return completableRawContributors.stream().map(listCompletableFuture -> {
            try {
                return listCompletableFuture.get();
            } catch (ExecutionException | InterruptedException e) {
                e.printStackTrace();
                throw new ResponseStatusException(
                        HttpStatus.INTERNAL_SERVER_ERROR, "An error occurred while getting contributors", e);
            }
        }).flatMap(Collection::stream).collect(Collectors.toList());
    }

    /**
     * Groups contributions by his/her login
     * @param rawContributors not grouped list of Contributor objexts
     * @return a map with key as a contributor login and value as a list of number of contributions
     */
    public Map<String, List<Integer>> groupByRawContributorsByName(List<Contributor> rawContributors) {
        Map<String, List<Integer>> contributorToContributions = rawContributors.stream().collect(
                Collectors.groupingBy(Contributor::getLogin,
                        Collectors.mapping(Contributor::getContributions, Collectors.toList()))
        );
        return contributorToContributions;
    }

    /**
     * Constructs a list of Contributor objects, where every contributor (identified by login/name) has a sum of contributions
     * in separate list by repository, sorted in descending order.
     * @param contributorToContributions a map with key as a contributor login and value as a list of number of contributions
     * @return a list of Contributor objects, with a summed number of contributions, sorted in descending order.
     */
    public List<Contributor> reduceContributionsAndSort(Map<String, List<Integer>> contributorToContributions) {
        return contributorToContributions.entrySet().stream().map(entry -> {
            int contributions = entry.getValue().stream().reduce(0, (acc, element) -> acc + element);
            return new Contributor(entry.getKey(), contributions);
        }).sorted(Comparator.comparingInt(Contributor::getContributions).reversed()).collect(Collectors.toList());
    }

    @Async
    private CompletableFuture<List<Contributor>> getContributorsByOrgAndRepo(String orgName, String repo) throws IOException, InterruptedException {
        return CompletableFuture.completedFuture(
               requestService.callPaginatedRequest(DOMAIN + "repos/" + orgName + "/" + repo + "/contributors", Contributor.class)
        );
    }

    // For UT purpose
    public void setRequestService(RequestService requestService) {
        this.requestService = requestService;
    }
}
