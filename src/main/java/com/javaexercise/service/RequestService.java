package com.javaexercise.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Base64;
import java.util.LinkedList;
import java.util.List;

@Service
public class RequestService {

    @Value("${GH_TOKEN}")
    private String ghToken;
    @Value("${GH_LOGIN}")
    private String ghLogin;

    @Autowired
    private Serializer serializer;

    /**
     * Performs a sequence of requests, each for a next page, until the requested page (response) is empty.
     * It means that it's the last page
     * @param url
     * @param clazz
     * @return a list of objects with object of class ${clazz}s
     * @throws IOException
     * @throws InterruptedException
     */
    public List callPaginatedRequest(String url, Class clazz) throws IOException, InterruptedException {
        List partialList = new LinkedList();
        List completeList = new LinkedList();

        HttpClient client = HttpClient.newHttpClient();

        int pageNo = 1;
        do {
            completeList.addAll(partialList);

            HttpRequest request = getRequestDraft()
                    .uri(URI.create(url + "?page=" + pageNo))
                    .build();
            HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            partialList = serializer.serialize(response, clazz);
            pageNo++;
        } while (!partialList.isEmpty());
        return completeList;
    }

    private HttpRequest.Builder getRequestDraft() {
        return HttpRequest.newBuilder()
                .header("Authorization", basicAuth(ghLogin, ghToken))
                .header("accept", "application/json");
    }

    private String basicAuth(String username, String password) {
        return "Basic " + Base64.getEncoder().encodeToString((username + ":" + password).getBytes());
    }

    // For UT purpose
    public void setSerializer(Serializer serializer) {
        this.serializer = serializer;
    }
}
